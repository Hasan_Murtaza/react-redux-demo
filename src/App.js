import React from 'react';
import logo from './logo.svg';
import './App.css';
import HeaderContainer from './containers/HeaderContainers'
import HomeContainers from './containers/HomeContainers';

function App() {
  return (
    <div className="App">
      <HeaderContainer />
    <HomeContainers />
    </div>
  );
}

export default App;
