import React from 'react'

//  function Home(props) {
    class Home extends React.Component{
        render(){
            console.warn('props', this.props)
            return (
                <div>
                <div className="add-to-cart">
            <span className="cart-count">{this.props.data.length}</span>
        <img src="https://static.vecteezy.com/system/resources/thumbnails/000/496/007/small/Ecommerce_998.jpg" />
    </div>

<h1>React Redux Component</h1>
<div className="cart-wrapper">
    <div className="img-wrapper item">
        <img src="https://www.fdfproject.com/wp-content/uploads/2018/12/iphone-png.png" />
    </div>
    <div className="text-wrapper item">
        <span>
            I-Phone
        </span>
        <span>
            Price: $1000.00
        </span>
    </div>
    <div className="btn-wrapper item">
        <button onClick={
                        ()=>{this.props.addToCartHandler({pice:1000,name:'i phone 11'})}
                        }>Add To Cart</button>
        
    </div>
</div>
<br/>
<div className="cart-wrapper">
    <div className="img-wrapper item">
        <img src="https://www.fdfproject.com/wp-content/uploads/2018/12/iphone-png.png" />
    </div>
    <div className="text-wrapper item">
        <span>
            Sumsung
        </span>
        <span>
            Price: $10000.00
        </span>
    </div>
    <div className="btn-wrapper item">
        <button onClick={
                        ()=>{this.props.addToCartHandler({pice:10000,name:'Sumsung'})}
                        }>Add To Cart</button>
        
    </div>
</div>
</div>
            )
        }

    
}

export default Home